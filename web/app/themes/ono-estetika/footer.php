<?php
/**
 * Theme Footer
 *
 * @package    WordPress
 * @subpackage ono-estetika
 * @since      ono-estetika 1.0
 */
?>
			<?php echo get_part('components/footer/index'); ?>
			<?php wp_footer(); ?>
		</div> <!-- End of #page -->
	</body>
</html>

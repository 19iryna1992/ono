<?php
/**
 * Theme index file.
 * @package    WordPress
 * @subpackage ono-estetika
 * @since      ono-estetika 1.0
 */
get_header();

?>
<main id="page-content" role="main" class="page-content page-content--index">
	<div id="content" tabindex="-1" class="page-content__wrapper">
		<?php get_template_part('parts/flexible-content/flexible'); ?>
	</div>
</main>
<?php
get_footer();

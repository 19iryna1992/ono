<?php

/**
 * Theme Header
 *
 * @package    WordPress
 * @subpackage ono-estetika
 * @since      ono-estetika 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,900;1,400;1,700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
	<?php settings_colors('styles'); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page">
		<a class="skip-link" href="#content"><?php esc_html_e('Skip to content', 'ono-estetika'); ?></a>
		<?php echo get_part('components/header/index'); ?>
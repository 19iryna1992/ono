let scrollPosition = 0;

function toggleMenu(e) {
	e.preventDefault();
	const el = document.querySelectorAll(".btn-menu");
	let isClose = false;

	el.forEach(btn => {
		const nav = document.querySelector(".JS--main-header__nav");
		const header = document.querySelector(".JS--main-header__container");
		const headerBackground = document.querySelector(".JS--main-header__nav-bg");


		if (btn.classList.contains('open') && headerBackground.style.transform !== 'translateX(-100%)') {
			btn.classList.remove('open');
			nav.classList.remove('open');
			header.classList.remove('open');
			headerBackground.style.opacity = '0';
			headerBackground.style.transform = 'translateX(-100%)';
			setTimeout(() => headerBackground.style.transform = '', 250);

			isClose = true;
		}
		else if (btn.classList.contains('open') == false && headerBackground.style.transform === '') {
			btn.classList.add('open');
			nav.classList.add('open');
			header.classList.add('open');
			headerBackground.style.opacity = '1';
			headerBackground.style.transform = 'translateX(0)';

		}

	});

	const wpAdminbar = document.getElementById('wpadminbar');

	let addHeight = 0;

	if (wpAdminbar) {
		addHeight = wpAdminbar.scrollHeight;
	}

	if (isClose) {
		document.body.style.removeProperty('overflow');
		document.body.style.removeProperty('position');
		document.body.style.removeProperty('top');
		document.body.style.removeProperty('width');

		window.scrollTo(0, scrollPosition);
	}
	else {
		scrollPosition = window.pageYOffset;

		document.body.style.overflow = 'hidden';
		document.body.style.position = 'fixed';
		document.body.style.top = `-${scrollPosition - addHeight}px`;
		document.body.style.width = '100%';
	}
}

function stickyMenu(e) {
	e.preventDefault();
	const headerContainer = document.querySelector(".JS--main-header__container");
	const btn = document.querySelector(".btn-menu");

	if (document.body.scrollTop >= 70 || document.documentElement.scrollTop > 70) {
		headerContainer.classList.add('sticky');
		btn.classList.add('btn-menu--sticky');
	} else {
		if (document.body.style.overflow != 'hidden') {
			headerContainer.classList.remove('sticky');
			btn.classList.remove('btn-menu--sticky');
		}
	}
}

const runHeader = () => {
	const element = document.querySelectorAll(".btn-menu");

	element.forEach(e => {
		e.addEventListener('click', toggleMenu);
	});
	document.addEventListener('scroll', stickyMenu)
}

runHeader();



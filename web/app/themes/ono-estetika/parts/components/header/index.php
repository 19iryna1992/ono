<?php
global $post;

$post_ID = !empty($post) ? $post->ID : 0;

if (is_404()) {
	$post_ID = get_field('404_page_content', 'options');
} else if (is_home()) {
	$post_ID = get_option('page_for_posts');
}

// ACF
$header_style = get_field('header_style', $post_ID) ?  get_field('header_style', $post_ID) : 'style_1';
$main_logo    = get_field('header_logo_primary', 'option');
$second_logo  = get_field('header_logo_second', 'option');
$third_logo   = get_field('header_logo_third', 'option');
$header_link  = get_field('header_link', 'option');

load_blocks_script('header', 'ono-estetika/header');
?>
<header class="main-header <?php esc_attr_e($header_style); ?>">
	<?php load_inline_styles(__DIR__, 'header'); ?>
	<div class="main-header__container JS--main-header__container">
		<div class="container">
			<div class="row justify-content-between">
				<div class="col-auto main-logo__flex-wrap">
					<a href="<?php echo get_bloginfo('url'); ?>" class="main-logo">
						<span class="screen-reader-text"><?php _e('Main Logo', 'ono-estetika'); ?></span>
						<?php
						switch ($header_style) {
							case 'style_1':
								echo get_img($main_logo);
								break;
							case 'style_2':
								echo get_img($second_logo);
								break;
							case 'style_3':
								echo get_img($third_logo);
								break;
						}
						?>
					</a>
				</div>
				<div class="col-auto">
					<div class="main-nav__right">
						<div class="main-nav__wrap">
							<?php if ($header_link) : ?>
								<?php echo get_button($header_link, '', 'main-header__btn'); ?>
							<?php endif ?>
						</div>
						<button class="btn-menu block-show--down">
							<span></span>
							<span></span>
							<span></span>
							<span class="screen-reader-text"><?php _e('Menu', 'ono-estetika'); ?></span>
						</button>
						<div class="main-header__lang">
							<?php echo do_shortcode('[wpml_language_selector_widget]') ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row main-nav__wrap--mob">
				<div class="col-12">
					<?php if ($header_link) : ?>
						<?php echo get_button($header_link, '', 'main-header__btn'); ?>
					<?php endif ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="main-header__nav JS--main-header__nav">
						<div class="main-header__nav__bg JS--main-header__nav-bg"></div>
						<nav class="main-nav" aria-label="<?php _e('Main Navigation', 'ono-estetika'); ?>">
							<?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => false)); ?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
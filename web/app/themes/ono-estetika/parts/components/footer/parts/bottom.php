<?php

// ACF
$copyrights = get_field('copyrights', 'option');

?>

<div class="container">
    <div class="footer__bottom block-show--left">
        <?php if ($copyrights) : ?>
            <div class="footer__copyrights">
                <span>© <?php echo date("Y") . ' ' . $copyrights ?></span>
            </div>
        <?php endif ?>
        <?php if (have_rows('privacy_links', 'option')) : ?>
            <div class="footer__privacy">
                <?php while (have_rows('privacy_links', 'option')) : the_row();
                    $link = get_sub_field('link');
                ?>
                    <?php if ($link) : ?>
                        <a class="footer__link" href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
                    <?php endif ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
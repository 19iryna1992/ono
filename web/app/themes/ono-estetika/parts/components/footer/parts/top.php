<?php

// ACF
$first_menu = get_field('footer_menu_first', 'option');
$second_menu = get_field('footer_menu_second', 'option');
$logo = get_field('footer_logo', 'option');



?>

<div class="container">
    <div class="row justify-content-between">
        <?php if ($logo) : ?>
            <div class="col-12 col-lg-4 order-lg-1">
                <div class="footer__logo block-show--up">
                    <?php echo wp_get_attachment_image($logo, 'full'); ?>
                </div>
            </div>
        <?php endif ?>
        <div class="col-12 col-lg-8">
            <nav class="footer__nav block-show--up delay--10">
                <?php echo wp_nav_menu(array(
                    'menu' => $first_menu,
                    'container' => false
                )) ?>
            </nav>
            <nav class="footer__nav block-show--up delay--20">
                <?php echo wp_nav_menu(array(
                    'menu' => $second_menu,
                    'container' => false
                )) ?>
            </nav>
        </div>
    </div>
    <?php if (have_rows('social_links', 'option')) : ?>
        <div class="row block-show--up">
            <div class="col-12">
                <ul class="footer__social-list">
                    <?php while (have_rows('social_links', 'option')) : the_row();
                        $icon = get_sub_field('icon', 'option');
                        $link = get_sub_field('link','option')
                    ?>
                        <li class="footer__social-item">
                            <a href="<?php echo $link ?>">
                            <?php echo wp_get_attachment_image($icon, 'full'); ?>
                            </a>
                        </li>
                    <?php endwhile; ?>
                </ul>

            </div>
        </div>
    <?php endif; ?>
</div>
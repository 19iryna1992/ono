<?php
	$post_title = '';
	$first_name = get_field( 'first_name' );
	// $last_name  = get_field( 'last_name' );


	if ( ! empty( $first_name ) ) {
		$post_title .= $first_name;
	}
	// if ( ! empty( $last_name ) ) {
	// 	if ( ! empty( $first_name ) ) {
	// 		$post_title .= '<br>';
	// 	}
	// 	$post_title .= $last_name;
	// }
	if ( empty( $post_title ) ) {
		$post_title = get_the_title();
	}
?>

<article <?php post_class( 'post-grid post-grid--post col-12 col-md-6 col-lg-4 block-show--up' ); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-grid__thumb">
			<a class="post-grid__thumb__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail( 'grid-doctor' ); ?>
			</a>
		</div>
	<?php endif; ?>
	<?php if ( ! empty( $categories = get_the_category_list( ', ' ) ) ) : ?>
		<div class="post-grid__categories">
			<?php echo $categories; ?>
		</div>
	<?php endif; ?>
	<header class="post-grid__heading">
		<h3 class="post-grid__title"><a class="post-grid__title__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo $post_title; ?></a></h3>
	</header>
	<div class="post-grid__excerpt">
		<?php the_excerpt(); ?>
	</div>
	<a class="post-grid__more" href="<?php the_permalink(); ?>"><?php _e( 'More', 'ono-estetika' ); ?></a>
</article>
<?php
$phone = get_field('phone');
$email = get_field('e-mail');
$address = get_field('address');
$button = get_field('button');
?>

<article <?php post_class('post-grid post-grid--objective col-12 col-md-6 col-lg-4 block-show--up'); ?>>
	<?php if (has_post_thumbnail()) : ?>
		<div class="post-grid__thumb">
			<a class="post-grid__thumb__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail('grid'); ?>
			</a>
		</div>
	<?php endif; ?>
	<header class="post-grid__heading">
		<h3 class="post-grid__title"><a class="post-grid__title__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
	</header>
	<div class="post-grid__info">
		<?php
		if (!empty($phone)) :
		?><p class="phone"><?php echo make_phone_clickable($phone); ?></p>
		<?php
		endif;

		if (!empty($email)) :
		?><p class="email"><?php echo make_email_clickable($email); ?></p>
		<?php
		endif;

		if (!empty($address)) :
			echo $address;
		endif; ?>
	</div>
	<?php
	if (!empty($button)) :
		echo get_button($button);
	endif;
	?>

</article>
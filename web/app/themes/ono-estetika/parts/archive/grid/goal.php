<article <?php post_class( 'post-grid post-grid--goal col-12 col-md-6 col-lg-4 block-show--up' ); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-grid__thumb">
			<a class="post-grid__thumb__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php the_post_thumbnail( 'grid' ); ?>
			</a>
		</div>
	<?php endif; ?>
	<header class="post-grid__heading">
		<h3 class="post-grid__title"><a class="post-grid__title__url" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
	</header>
</article>
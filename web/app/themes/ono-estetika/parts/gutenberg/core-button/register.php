<?php
function insert_button_styles( $block_content, $block ) {
	if ( 'core/button' === $block['blockName'] ) {
		$block_content = '<div data-key="core-button"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'insert_button_styles', 10, 2 );

function extend_block_button_script() {
	wp_enqueue_script(
		'ono-estetika/block_button_script',
		get_template_directory_uri() . '/parts/gutenberg/core-button/admin.min.js',
		array( 'wp-i18n', 'wp-dom-ready', 'wp-editor', 'wp-blocks' )
	);
}
add_action( 'enqueue_block_editor_assets', 'extend_block_button_script' );
<?php
function insert_media_text_styles( $block_content, $block ) {
	if ( 'core/media-text' === $block['blockName'] ) {
		$block_content = '<div data-key="core-media-text"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'insert_media_text_styles', 10, 2 );
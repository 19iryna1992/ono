/*global wp*/
const { __ } = wp.i18n;
const {registerBlockStyle} = wp.blocks;

const styles = [
    {
        name: 'uppercase',
        label: __('Uppercase', 'onoEstetika')
    },
    {
        name: 'subheading',
        label: __('Subheading', 'onoEstetika')
    },
    {
        name: 'leadparagraph',
        label: __('Leadparagraph', 'onoEstetika')
    },
];

wp.domReady(() => {
    styles.forEach(style => {
        registerBlockStyle('core/paragraph', style);
    });
});
<?php
/**
 * Block CTA
 *
 * @package WordPress
 * @subpackage ono-estetika
 * @since ono-estetika 1.0
 */
$block_object = new Block( $block );
$attr = $block_object->attr();
$name = $block_object->name();

?>
<section <?php echo $attr; ?>>
	<?php echo load_inline_styles( __DIR__, $name ); ?>
	<div class="cta__content">
		<InnerBlocks />
	</div>
</section>
<?php
function insert_image_styles( $block_content, $block ) {
	if ( 'core/image' === $block['blockName'] ) {
		$block_content = '<div data-key="core-image"></div>' . $block_content;
	}
	return $block_content;
}
add_filter( 'render_block', 'insert_image_styles', 10, 2 );
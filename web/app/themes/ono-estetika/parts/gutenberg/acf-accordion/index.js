function toggleAccordion(e) {
	e.preventDefault();
	const parent = e.target.closest('.single-accordion__item');

	if (parent.classList.contains('active')) {
		parent.classList.remove('active');
	} else {
		parent.classList.add('active');
	}
}

const runAccordions = () => {
	const elements = document.querySelectorAll(".single-accordion__trigger")

	Array.from(elements).forEach( (element) => {
		element.addEventListener('click', toggleAccordion);
	});
}

runAccordions();

if ( window.acf ) {
    window.acf.addAction( 'render_block_preview/type=accordion', runAccordions);
}

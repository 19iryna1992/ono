<?php
/**
 * Block with Accordion
 *
 * @package WordPress
 * @subpackage ono-estetika
 * @since ono-estetika 1.0
 */
$block_object = new Block( $block );
$attr = $block_object->attr();
$name = $block_object->name();

if ( have_rows('accordion_row') ) :
?>
<section <?php echo $attr; ?>>
	<?php echo load_inline_styles( __DIR__, $name ); ?>
	<header class="accordion__header">
		<div class="container">
			<?php $block_object->title('accordion__title'); ?>
			<?php $block_object->desc('accordion__description'); ?>
		</div>
	</header>
	<div class="container accordion__container">
		<div class="single-accordion">
			<ul class="single-accordion__row">
			<?php
			while ( have_rows('accordion_row') ) : the_row();
				$title = get_sub_field('title');
				$content = get_sub_field('content');
				$single_class = "single-accordion__item";

				if ( !empty( $content ) ) :
			?>
				<li class="<?php echo $single_class; ?>">
					<button class="single-accordion__trigger">
						<span class="single-accordion__icon"></span>
					<?php if ( !empty( $title ) ) : ?>
						<span class="single-accordion__text"><?php echo $title; ?></span>
					<?php endif; ?>
					</button>
					<div class="single-accordion__content">
						<?php echo $content; ?>
					</div>
				</li>
			<?php
				endif;
			endwhile;
			?>
			</ul>
		</div>
	</div>
</section>
<?php
endif;

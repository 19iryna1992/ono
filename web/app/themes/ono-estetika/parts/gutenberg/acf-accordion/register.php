<?php
$name = 'accordion';
$title = str_replace( '_', ' ', ucfirst($name) );

acf_register_block(array(
    'name'              => $name,
    'title'             => __($title, 'ono-estetika'),
    'description'       => __("$title block", 'ono-estetika'),
    'category'          => 'theme_blocks',
    'icon'              => 'welcome-widgets-menus',
    'mode'              => 'edit',
    'align'             => false,
    'keywords'          => array($title, 'content'),
    'supports'          => array(
        'align' => false,
        'anchor' => true,
    ),
    'render_template'   => get_template_directory() . "/parts/gutenberg/acf-$name/index.php",
    'enqueue_assets'  => function() use ( $name ){
        wp_enqueue_script(
            "ono-estetika/$name",
            get_template_directory_uri() . "/parts/gutenberg/acf-$name/index.min.js",
            [],
            false,
            true
        );
    }
));
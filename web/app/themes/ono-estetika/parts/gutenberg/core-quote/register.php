<?php
add_filter( 'render_block', 'wrap_quote_block', 10, 2 );
function wrap_quote_block( $block_content, $block ) {
	if ( 'core/quote' === $block['blockName'] ) {
		$block_content = '<div data-key="core-quote"></div>' . $block_content;
	}
	return $block_content;
}

function add_core_block_styles_admin() {
	if( is_admin() ) {
		wp_enqueue_style(
			'core/quote',
			get_template_directory_uri() . '/parts/gutenberg/core-quote/style-editor.css',
			array( 'wp-edit-blocks' )
		);
	}
}
add_action( 'enqueue_block_editor_assets', 'add_core_block_styles_admin' );
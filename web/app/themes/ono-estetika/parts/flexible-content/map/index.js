(function ($) {

  /**
    * initMap
    *
    * Renders a Google Map onto the selected jQuery element
    *
    * @date    22/10/19
    * @since   5.8.6
    *
    * @param   jQuery $el The jQuery element.
    * @return  object The map instance.
    */
  function initMap($el) {

    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
      zoom: $el.data('zoom') || 11,
      center: {
        lat: parseFloat('46.4143602205275'),
        lng: parseFloat('6.585546678174183'),
      },
      scrollwheel: false,
      draggable: true,
      mapTypeControl: false,
      scaleControl: true,
      fullscreenControl: false,
      zoomControl: true,
      streetViewControl: false,
      rotateControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{ "elementType": "geometry", "stylers": [{ "color": "#e5bba9" }] }, { "elementType": "labels.text.fill", "stylers": [{ "color": "#523735" }] }, { "elementType": "labels.text.stroke", "stylers": [{ "color": "#f5f1e6" }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#c9b2a6" }] }, { "featureType": "administrative.land_parcel", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative.land_parcel", "elementType": "geometry.stroke", "stylers": [{ "color": "#dcd2be" }] }, { "featureType": "administrative.land_parcel", "elementType": "labels.text.fill", "stylers": [{ "color": "#ae9e90" }] }, { "featureType": "administrative.neighborhood", "stylers": [{ "visibility": "off" }] }, { "featureType": "landscape.natural", "elementType": "geometry", "stylers": [{ "color": "#f6d3c4" }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#d78a75" }] }, { "featureType": "poi", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "poi", "elementType": "labels.text.fill", "stylers": [{ "color": "#93817c" }] }, { "featureType": "poi.park", "elementType": "geometry.fill", "stylers": [{ "color": "#d78a75" }] }, { "featureType": "poi.park", "elementType": "labels.text.fill", "stylers": [{ "color": "#447530" }] }, { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#f1f0f1" }] }, { "featureType": "road", "elementType": "labels", "stylers": [{ "visibility": "off" }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#d6a28f" }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#d6a28f" }] }, { "featureType": "road.local", "elementType": "labels.text.fill", "stylers": [{ "color": "#806b63" }] }, { "featureType": "transit.line", "elementType": "geometry", "stylers": [{ "color": "#ffc894" }] }, { "featureType": "transit.line", "elementType": "labels.text.fill", "stylers": [{ "color": "#8f7d77" }] }, { "featureType": "transit.line", "elementType": "labels.text.stroke", "stylers": [{ "color": "#ebe3cd" }] }, { "featureType": "water", "elementType": "geometry.fill", "stylers": [{ "color": "#f7e2d9" }] }, { "featureType": "water", "elementType": "labels.text", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "labels.text.fill", "stylers": [{ "color": "#92998d" }] }]
    };

    var map = new google.maps.Map($el[0], mapArgs);

    // Add markers.
    map.markers = [];
    $markers.each(function () {
      initMarker($(this), map);
    });

    // Center map based on markers.
    centerMap(map);

    // Return map instance.
    return map;
  }

  /**
   * initMarker
   *
   * Creates a marker for the given jQuery element and map.
   *
   * @date    22/10/19
   * @since   5.8.6
   *
   * @param   jQuery $el The jQuery element.
   * @param   object The map instance.
   * @return  object The marker instance.
   */
  var infoWindows = [];
  function initMarker($marker, map) {
    var markerSvg = [
      '<svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">',
      '<circle cx="14.6263" cy="14" r="13" fill="#F1F0F1" stroke="#2F140D" stroke-width="2"/>',
      '<circle cx="14.6263" cy="14" r="10" fill="#2F140D"/></svg>'

    ].join('\n');

    var iconImage = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(markerSvg);

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var icon = {
      url: iconImage,
      scaledSize: new google.maps.Size(42, 42)
    };

    var latLng = {
      lat: parseFloat(lat),
      lng: parseFloat(lng)
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      icon: icon
    });

    // Append to reference for later use.
    map.markers.push(marker);

    // If marker contains HTML, add it to an infoWindow.
    if ($marker.html()) {

      // Create info window.
      var infoWindow = new google.maps.InfoWindow({
        content: $marker.html()
      });

      infoWindows.push(infoWindow);

      // Show info window when marker is clicked.
      google.maps.event.addListener(marker, 'click', function () {

        //close all
        for (var i = 0; i < infoWindows.length; i++) {
          infoWindows[i].close();
        }
        infoWindow.open(map, marker);
      });

      google.maps.event.addListener(map, 'click', function () {
        infoWindow.close();
      });

    }
  }

  /**
   * centerMap
   *
   * Centers the map showing all markers in view.
   *
   * @date    22/10/19
   * @since   5.8.6
   *
   * @param   object The map instance.
   * @return  void
   */
  function centerMap(map) {

    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function (marker) {
      bounds.extend({
        lat: marker.position.lat() + 0.0001,
        lng: marker.position.lng()
      });
    });

    // Case: Single marker.
    if (map.markers.length == 0) {
      map.setCenter({
        lat: parseFloat('46.4143602205275'),
        lng: parseFloat('6.585546678174183')
      }, 11);

    } else if (map.markers.length == 1) {
      map.setCenter(bounds.getCenter());

      // Case: Multiple markers.
    } else {
      map.fitBounds(bounds);

      // or for change zoom:
      // map.setCenter(bounds.getCenter());
      // map.setZoom(11);
    }
  }

  // Render maps on page load.
  $(document).ready(function () {
    $('.js--map').each(function () {
      var map = initMap($(this));
    });
  });

})(jQuery);
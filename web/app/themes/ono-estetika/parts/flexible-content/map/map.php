<?php

// ACF
$map_api = get_field('google_maps_api_key', 'option');
$clinics = get_sub_field('select_locations');
$title = get_sub_field('title');
$description = get_sub_field('description');
$link = get_sub_field('link');
$section_id = get_sub_field('section_id');

wp_enqueue_script('map-google-googleapis', 'https://maps.googleapis.com/maps/api/js?key=' . $map_api, array(), '', true);
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="map">
	<?php load_inline_styles(__DIR__, 'map'); ?>
	<?php load_blocks_script('map', 'ono-estetika/map', ['jquery', 'map-google-googleapis'], 'parts/flexible-content'); ?>
	<div class="map__info">
		<?php if ($title) : ?>
			<h2 class="map__title has-line-animation">
				<?php echo $title ?>
			</h2>
		<?php endif ?>
		<?php if ($description) : ?>
			<div class="map__description has-line-animation--wrap">
				<?php echo $description ?>
			</div>
		<?php endif ?>
		<?php if ($link) : ?>
			<?php echo get_button($link, 'simple', 'map__link has-line-animation'); ?>
		<?php endif; ?>
	</div>
	<div class="map__container js--map" data-zoom="11">
		<?php if ($clinics) : ?>
			<?php foreach ($clinics as $post_ID) :
				$location = get_field('clinic_location', $post_ID);
				$clinic_name = get_the_title($post_ID);
				$clinic_description = get_the_excerpt($post_ID);
				$clinic_link = get_permalink($post_ID);
			?>
				<div class="map__marker marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>">
					<a class="map__modal-title" href="<?php echo $clinic_link ?>">
						<h3 class="map__modal-title"><?php echo $clinic_name ?></h3>
					</a>
					<div class="map__modal-description">
						<?php
						if (!empty($phone = get_field('phone', $post_ID))) :
						?><p class="phone"><?php echo make_phone_clickable($phone); ?></p>
						<?php
						endif;

						if (!empty($email = get_field('e-mail', $post_ID))) :
						?><p class="email"><?php echo make_email_clickable($email); ?></p>
						<?php
						endif;

						if (!empty($address = get_field('address', $post_ID))) :
							echo $address;
						endif;

						if (!empty($schedules = get_field('schedules', $post_ID))) :
							echo $schedules;
						endif;
						?>
					</div>
					<?php
					if (!empty($button = get_field('button', $post_ID))) :
						echo get_button($button);
					endif;
					?>
				</div>
			<?php endforeach ?>
		<?php endif ?>
	</div>
</section>
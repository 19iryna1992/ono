<?php

// ACF

$style_section = get_sub_field('section_style');
$section_title = get_sub_field('section_title');
$text          = get_sub_field('section_description');
$link          = get_sub_field('link');
$section_id = get_sub_field('section_id');
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="arguments <?php echo $style_section === 'style_1' ? 'style_1' : 'style_2' ?>">
	<?php load_inline_styles(__DIR__, 'arguments'); ?>
	<div class="container">
		<?php if ($style_section === 'style_1') : ?>
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="arguments__content">
						<?php if ($section_title) : ?>
							<h2 class="arguments__title has-line-animation has-gradient"><?php echo $section_title ?></h2>
						<?php endif; ?>
						<?php if ($text) : ?>
							<div class="arguments__description has-line-animation--wrap">
								<?php echo $text ?>
							</div>
						<?php endif; ?>
						<?php if ($link) : ?>
							<div class="arguments__link-wrap">
							<a class="arguments__link has-line-animation" href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<?php if (have_rows('argument_boxes')) : ?>
					<div class="col-12 col-lg-6">
						<div class="row argument__boxes">
							<?php while (have_rows('argument_boxes')) : the_row();
								$icon        = get_sub_field('icon');
								$title       = get_sub_field('title');
								$description = get_sub_field('description');
							?>
								<div class="col-6 argument__box block-show--up">
									<div class="argument__box-icon">
										<?php echo get_img($icon); ?>
									</div>
									<?php if ($title) : ?>
										<h5 class="argument__box-title"><?php echo $title ?></h5>
									<?php endif; ?>
									<?php if ($description) : ?>
										<div class="argument__box-description">
											<?php echo $description ?>
										</div>
									<?php endif; ?>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php else : ?>
			<div class="row">
				<?php while (have_rows('argument_boxes')) : the_row();
					$icon = get_sub_field('icon');
					$title = get_sub_field('title');
					$description = get_sub_field('description');
				?>
					<div class="col-6 col-lg-3 argument__box block-show--up">
						<div class="argument__box-icon">
							<?php echo get_img($icon); ?>
						</div>
						<?php if ($title) : ?>
							<h5 class="argument__box-title"><?php echo $title ?></h5>
						<?php endif; ?>
						<?php if ($description) : ?>
							<div class="argument__box-description">
								<?php echo $description ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</section>
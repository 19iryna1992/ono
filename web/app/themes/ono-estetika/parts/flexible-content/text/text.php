<?php
// ACF
$section_style = get_sub_field('section_style');
$title = get_sub_field('section_title');
$text = get_sub_field('text');
$text_second = get_sub_field('text_second');
$link = get_sub_field('link');
$section_id = get_sub_field('section_id');

?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="text">
    <?php load_inline_styles(__DIR__, 'text'); ?>
    <div class="container">
        <div class="row">
            <?php if ($title) : ?>
                <div class="col-12">
                    <h2 class="text__title has-line-animation has-gradient"><?php echo $title ?></h2>
                </div>
            <?php endif; ?>
        </div>
        <div class="row text__content-container">
            <?php if ($text) : ?>
                <div class="col-12 <?php echo $section_style === 'style_2' ? 'col-lg-6 style_2' : 'style_1' ?> text__content has-line-animation--wrap">
                    <?php echo $text ?>
                </div>
            <?php endif; ?>
            <?php if ($text_second && $section_style === 'style_2') : ?>
                <div class="col-12 col-lg-6 text__content style_2 has-line-animation--wrap">
                    <?php echo $text_second ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <?php if ($link) : ?>
                <div class="col-12">
                    <div class="text__link-wrap">
                        <a class="text__link has-line-animation" href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>"><?php echo $link['title'] ?></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
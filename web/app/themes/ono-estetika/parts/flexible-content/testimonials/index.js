function runSlider() {
	const swiper = new Swiper('.slider__container', {
		loop: true,
		autoHeight: true,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
	});
}

document.addEventListener( 'DOMContentLoaded', () => {
	runSlider();
});
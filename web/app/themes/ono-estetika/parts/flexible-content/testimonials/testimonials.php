<?php

$section_id = get_sub_field('section_id');

$testimonials_args = array(
    'post_type'      => 'testimonial',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
);

if ( ! empty( $testimonials = get_sub_field( 'testimonials' ) ) ) {
    unset( $testimonials_args['posts_per_page'] );

    $testimonials_args['post__in'] = wp_parse_id_list( $testimonials );
}

$testimonials_query = new WP_Query($testimonials_args);
?>

<?php if ($testimonials_query->have_posts()) : ?>
    <section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="testimonials">
        <?php load_inline_styles(__DIR__, 'testimonials'); ?>
        <?php load_inline_styles_plugin('swiper-bundle.min'); ?>
        <?php load_inline_styles_shared('sliders'); ?>
        <?php load_blocks_script('testimonials', 'ono-estetika/testimonials', ['swiper'], 'parts/flexible-content'); ?>
        <div class="container">
            <div class="testimonials__slider slider__container swiper-container block-show--up">
                <div class="testimonials__slider-row swiper-wrapper">

                    <?php while ($testimonials_query->have_posts()) :
                        $testimonials_query->the_post(); ?>
                        <div class="custom-slide swiper-slide">
                            <div class="testimonials__item">
                                <div class="testimonials__content">
                                    <?php echo get_the_content() ?>
                                </div>
                                <div class="testimonials__author">
                                    <div class="testimonials__name">
                                        <?php echo get_the_title() ?>
                                    </div>
                                    <div class="testimonials__description">
                                        <?php echo get_the_excerpt() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>

                    <?php wp_reset_postdata(); ?>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>
<?php endif; ?>
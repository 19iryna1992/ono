<?php

//ACF

$style_section = get_sub_field('section_style');
$position_content = get_sub_field('content_position');
$section_title = get_sub_field('section_title');
$text = get_sub_field('text');
$link = get_sub_field('link');
$img_main = get_sub_field('img_main');
$img_secondary = get_sub_field('img_secondary');
$section_id = get_sub_field('section_id');
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="text-img <?php echo $style_section === 'style_1' ? 'style_1' : 'style_2' ?> <?php echo $position_content === 'left' ? 'content-left' : 'content-right' ?>">
	<?php load_inline_styles(__DIR__, 'text-image'); ?>

	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-lg-6 <?php echo $position_content === 'left' ? 'order-1' : '' ?>">
				<figure class="text-img__box">
					<?php if ($img_main) : ?>
						<div class="text-img__main-photo rellax event-viewport" data-rellax-speed="3"><div class="block-show--clip"><?php echo get_img($img_main); ?></div></div>
					<?php endif; ?>
					<?php if ($img_secondary && $style_section === 'style_1') : ?>
						<div class="text-img__secondary-photo rellax event-viewport" data-rellax-speed="6"><div class="block-show--clip"><?php echo get_img($img_secondary); ?></div></div>
					<?php endif; ?>
					<div class="text-img__decoration">
						<div class="decoration">
							<?php echo get_img('line-decoration'); ?>
						</div>
					</div>
				</figure>
			</div>
			<div class="col-12 col-lg-6">
				<div class="text-img__content">
					<?php if ($section_title) : ?>
						<h2 class="text-img__title has-line-animation"><?php echo $section_title ?></h2>
					<?php endif; ?>
					<?php if ($text) : ?>
						<div class="text-img__description has-line-animation">
							<?php echo $text ?>
						</div>
					<?php endif; ?>
					<?php if ($link) : ?>
						<div class="text-img__link-wrap block-show--up">
							<?php echo get_button( $link, 'outline', 'text-img__link' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
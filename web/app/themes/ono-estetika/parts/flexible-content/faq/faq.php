<?php
//ACF
$section_title = get_sub_field('section_title');
$section_id = get_sub_field('section_id');

$faq_args = array(
    'post_type'      => 'faq',
    'post_status'    => 'publish',
    'posts_per_page' => -1,
);

if ( ! empty( $faq = get_sub_field( 'faq' ) ) ) {
    unset( $faq_args['posts_per_page'] );

    $faq_args['post__in'] = wp_parse_id_list( $faq );
}

$faq_query = new WP_Query($faq_args);
?>

<?php if ($faq_query->have_posts()) : ?>
    <section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="faq">
        <?php load_inline_styles(__DIR__, 'faq'); ?>
        <?php load_blocks_script('faq', 'ono-estetika/faq', [], 'parts/flexible-content'); ?>
        <div class="container">
            <div class="row justify-content-between">
                <?php if ($section_title) : ?>
                    <div class="col-12 col-lg-5 order-lg-2">
                        <h2 class="faq__title has-line-animation has-gradient"><?php echo $section_title ?></h2>
                    </div>
                <?php endif; ?>
                <div class="col-12 col-lg-6 order-lg-1">
                    <div class="faq__container JS--faq-container">
                        <?php while ($faq_query->have_posts()) :
                            $faq_query->the_post(); ?>
                            <button class="faq__button block-show--up JS--faq-btn"><?php echo get_the_title() ?></button>
                            <div class="faq__answer">
                                <div class="faq__answer-content">
                                    <?php echo get_the_content() ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="faq__decoration">
            <div class="decoration">
                <?php echo get_img('line-decoration'); ?>
            </div>
        </div>
    </section>
<?php
endif;
wp_reset_postdata();
?>
var accordionButtons = document.querySelectorAll(".JS--faq-btn");


accordionButtons.forEach(btn => {
    btn.addEventListener('click', (e) => {
        btn.classList.toggle('is-open')

        const content = btn.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    })

});


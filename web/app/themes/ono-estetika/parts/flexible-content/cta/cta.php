<?php

//ACF
$section_style = get_sub_field('section_style');
$bg_image = get_sub_field('bg_image');
$bg_image_opacity = get_sub_field('bg_image_opacity');
$title = get_sub_field('section_title');
$description = get_sub_field('section_description');
$link = get_sub_field('link');
$section_id = get_sub_field('section_id');
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="cta parallax__wrap <?php echo $section_style === 'style_1' ? 'style_1' : 'style_2' ?>">
	<?php load_inline_styles(__DIR__, 'cta'); ?>

	<?php if ( $bg_image ) : ?>
		<div class="cta__bg parallax__bg<?php echo $bg_image_opacity ? ' has-opacity' : ''; ?>"><?php echo get_img( $bg_image ); ?></div>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="cta__content">
					<?php if ($title) : ?>
						<h2 class="cta__title has-line-animation has-gradient"><?php echo $title ?></h2>
					<?php endif; ?>
					<?php if ($description) : ?>
						<div class="cta__description has-line-animation--wrap"><?php echo $description ?></div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-12">
				<?php if ($link) : ?>
					<div class="cta__link-wrap block-show--up">
					<?php echo get_button( $link, 'light', 'cta__link' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
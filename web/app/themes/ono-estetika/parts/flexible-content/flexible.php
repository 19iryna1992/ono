<?php
global $post;

$post_ID = ! empty( $post ) ? $post->ID : 0;

if ( is_404() ) {
	$post_ID = get_field( '404_page_content', 'options' );
}
else if ( is_home() ) {
	$post_ID = get_option( 'page_for_posts' );
}

if (have_rows('flexible_content', $post_ID)) :

	while (have_rows('flexible_content', $post_ID)) : the_row();
		$layout = get_row_layout();

		$layout = str_replace( 'section_', '', $layout );
		$layout = str_replace( '_', '-', $layout );

		get_template_part( "parts/flexible-content/$layout/$layout" );

	endwhile;
endif;

<?php

//ACF
$section_style    = get_sub_field('section_style');
$bg_image         = get_sub_field('bg_image');
$bg_image_mobile  = get_sub_field('bg_image_mobile');
$logo             = get_sub_field('logo');
$section_subtitle = get_sub_field('section_subtitle');
$section_title    = get_sub_field('section_title');
$link             = get_sub_field('link');
$section_id = get_sub_field('section_id');

$title_delay      = ($section_subtitle) ? ' delay--10' : '';
$button_delay     = ($section_title && $section_subtitle) ? ' delay--20' : ' delay--10';
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="hero parallax__wrap">
	<?php load_inline_styles(__DIR__, 'hero-banner'); ?>

	<?php if ($bg_image_mobile) : ?>
		<div class="hero__bg hero__bg--mobile block-show--scale">
			<?php echo get_img($bg_image_mobile); ?>
		</div>
	<?php endif; ?>
	<?php if ($bg_image) : ?>
		<div class="hero__bg hero__bg--desktop block-show--scale">
			<?php echo get_img($bg_image); ?>

			<div class="hero__bg__overlay rellax__bg block-show--scale" data-rellax-speed="-6"><?php echo get_img($bg_image); ?></div>
		</div>
	<?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php if ($logo && $section_style === 'style_1') : ?>
					<div class="hero__logo block-show--up"><?php echo get_img($logo, 'full'); ?>
					<?php endif; ?>
					<?php if ($section_subtitle && $section_style === 'style_2') : ?>
						<div class="hero__subtitle block-show--up"><?php echo $section_subtitle ?></div>
					<?php endif; ?>
					<?php if ($section_title && $section_style === 'style_2') : ?>
						<h1 class="hero__title block-show--up<?php esc_attr_e($title_delay); ?>"><?php echo $section_title ?></h1>
					<?php endif; ?>
					</div>
					<div class="col-12">
						<?php if ($link) : ?>
							<?php echo get_button($link, 'light', 'hero__link block-show--up' . esc_attr($button_delay)); ?>
						<?php endif; ?>
					</div>
			</div>
		</div>
</section>
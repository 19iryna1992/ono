<?php

//ACF
$section_title = get_sub_field('section_title');
$content =  get_sub_field('content');
$section_id = get_sub_field('section_id');
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="rich-text">
    <?php load_inline_styles(__DIR__, 'rich-text'); ?>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <?php if ($section_title) : ?>
                    <h2 class="rich-text__title"><?php echo $section_title ?></h2>
                <?php endif ?>
                <?php if ($content) : ?>
                    <div class="rich-text__container"><?php echo $content ?></div>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>
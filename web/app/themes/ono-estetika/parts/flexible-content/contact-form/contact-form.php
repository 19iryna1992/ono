<?php
// ACF

$title = get_sub_field('section_title');
$description = get_sub_field('section_description');
$form = get_sub_field('form');
$section_id = get_sub_field('section_id');

?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="contact-form has-svg-line">
    <?php load_inline_styles_shared('wpcf7'); ?>
    <?php load_inline_styles(__DIR__, 'contact-form'); ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <?php if ($title) : ?>
                    <h2 class="contact-form__title has-line-animation has-gradient"><?php echo $title; ?></h2>
                <?php endif; ?>
                <?php if ($description) : ?>
                    <div class="contact-form__description has-line-animation--wrap">
                        <?php echo $description; ?>
                    </div>
                <?php endif; ?>
                <?php if ($form) : ?>
                    <div class="contact-form__item block-show--up">
                        <?php echo do_shortcode($form) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="contact-form__decoration">
            <div class="decoration">
                <?php echo get_img('line-decoration-large'); ?>
            </div>
        </div>
    </div>
</section>
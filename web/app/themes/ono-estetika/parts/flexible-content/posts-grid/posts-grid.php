<?php
$name = 'posts-grid';

// ACF
$style       = get_sub_field('section_style');
$title       = get_sub_field('section_title');
$description = get_sub_field('section_description');
$post_type   = get_sub_field('post_type') ? get_sub_field('post_type')          : 'post';
$per_page    = get_sub_field('posts_per_page') ? get_sub_field('posts_per_page') : -1;
$link_more   = get_sub_field('link_more');
$paged       = get_query_var('paged') ? get_query_var('paged') : 1;
$section_id = get_sub_field('section_id');
$is_opened = !empty(get_sub_field('is_opened')) ? get_sub_field('is_opened') : false;

$args = [
	'post_type'     => [$post_type],
	'post_status'   => 'publish',
	'post_per_page' => $per_page,
	'paged'         => $paged
];

if ($post_type == 'doctor') {
	$args['meta_key'] = 'last_name';
	$args['orderby']  = 'meta_value';
	$args['order']    = 'ASC';
}

$block_query = new WP_Query($args);

?>

<section <?php echo $section_id ? 'id="' . $section_id . '"' : '' ?> class="<?php esc_attr_e(sprintf('%1$s %1$s--%2$s %1$s--%3$s', $name, $style, $post_type)); ?>">
	<?php load_inline_styles(__DIR__, $name); ?>
	<div class="container">
		<?php if (!empty($title) || !empty($description)) : ?>
			<div class="<?php esc_attr_e($name); ?>__header">
				<?php if (!empty($title)) : ?>
					<h2 class="<?php esc_attr_e($name); ?>__title has-line-animation has-gradient"><?php echo $title; ?></h2>
				<?php endif; ?>
				<?php if (!empty($description)) : ?>
					<div class="<?php esc_attr_e($name); ?>__description has-line-animation--wrap"><?php echo $description; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php load_blocks_script('posts-grid', 'ono-estetika/posts-grid', [], 'parts/flexible-content'); ?>

		<form action="<?php echo get_permalink(); ?>" method="POST" class="<?php esc_attr_e($name); ?>__form" autocomplete="off">
			<input type="hidden" name="post_type" value="<?php esc_attr_e($post_type); ?>" />
			<?php
			$display_count = false;

			switch ($post_type) {
				case 'treatment':
					$treatment_filters = get_sub_field('treatment_filters');

					if (!empty($treatment_filters)) $display_count = true;

					if (in_array('gender', $treatment_filters)) the_posts_grid_filter('treatment', 'taxonomy', 'gender', '', $is_opened);
					if (in_array('area', $treatment_filters)) the_posts_grid_filter('treatment', 'taxonomy', 'area', '', $is_opened);
					if (in_array('type', $treatment_filters)) the_posts_grid_filter('treatment', 'taxonomy', 'type', '', $is_opened);
					if (in_array('goal', $treatment_filters)) the_posts_grid_filter('treatment', 'taxonomy', 'goal', '', $is_opened);
					break;

				case 'goal':
					$goal_filters = get_sub_field('goal_filters');

					if (!empty($goal_filters)) $display_count = true;

					if (in_array('gender', $goal_filters)) the_posts_grid_filter('goal', 'taxonomy', 'gender', '', $is_opened);
					if (in_array('area', $goal_filters)) the_posts_grid_filter('goal', 'taxonomy', 'area', '', $is_opened);
					if (in_array('benefit', $goal_filters)) the_posts_grid_filter('goal', 'taxonomy', 'benefit', '', $is_opened);
					break;

				case 'doctor':
					$doctor_filters = get_sub_field('doctor_filters');

					if (!empty($doctor_filters)) $display_count = true;

					if (in_array('clinic', $doctor_filters)) the_posts_grid_filter('doctor', 'post_type', 'clinic', 'clinic', $is_opened);
					if (in_array('language-speaking', $doctor_filters)) the_posts_grid_filter('doctor', 'taxonomy', 'language-speaking', $is_opened);
					if (in_array('clinics', $doctor_filters)) the_posts_grid_filter('doctor', 'post_type', 'clinics', 'clinic', $is_opened);
					if (in_array('area', $doctor_filters)) the_posts_grid_filter('doctor', 'taxonomy', 'area', '', $is_opened);
					break;
			}
			?>
		</form>

		<div class="<?php esc_attr_e($name); ?>__posts">
			<?php if ($block_query->have_posts()) : ?>
				<?php if ($display_count) : ?>
					<div class="<?php esc_attr_e($name); ?>__results block-show--up">
						<?php
						$found_posts = $block_query->found_posts;

						echo sprintf(
							'%s %s %s',
							$found_posts,
							_n($post_type, $post_type . 's', $found_posts, 'ono-estetika'),
							_n('found', 'founds', $found_posts, 'ono-estetika')
						);
						?>
					</div>
				<?php endif; ?>
				<div class="<?php esc_attr_e($name); ?>__row row">
					<?php while ($block_query->have_posts()) : $block_query->the_post(); ?>
						<?php get_template_part('parts/archive/grid/' .  $post_type); ?>
					<?php endwhile; ?>

					<?php wp_reset_postdata(); ?>
				</div>
			<?php else : ?>
				<div class="<?php esc_attr_e($name); ?>__results <?php esc_attr_e($name); ?>__results--empty block-show--up"><?php _e('Sorry, nothing found.', 'ono-estetika'); ?></div>
			<?php endif; ?>
		</div>

		<?php
		if (is_home()) {
		?><div class="<?php esc_attr_e($name); ?>__pagination">
				<?php
				the_posts_pagination([
					'mid_size'           => 2,
					'prev_text'          => __('Previous', 'ono-estetika'),
					'next_text'          => __('Next', 'ono-estetika'),
					'screen_reader_text' => __('Archive Pagination', 'ono-estetika'),
				]);
				?>
			</div>
		<?php
		}

		if (!empty($link_more)) :
			echo get_button($link_more, 'light', $name . '__button block-show--up');
		endif;
		?>
	</div>
</section>
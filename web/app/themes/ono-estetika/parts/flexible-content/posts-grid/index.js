"use strict";


/**
 * Fetch Rest Data
 */
function postsGridRestData( postsGridElement ) {
	let formOutput = {};
	const formData = new FormData( postsGridElement.querySelector( '.posts-grid__form' ) );

	postsGridElement.classList.add( 'loading' );

	formData.forEach( ( value, key ) => {
		if ( ! Reflect.has( formOutput, key ) ) {
			formOutput[key] = value;

			return;
		}

		if ( ! Array.isArray( formOutput[key] ) ) {
			formOutput[key] = [formOutput[key]];
		}

		formOutput[key].push( value );
	} );

	fetch( vars.restUrl + 'posts-grid/v1/posts', {
		method: 'POST',
		body: JSON.stringify( formOutput )
	})
		.then( response => response.text())
		.catch( error => console.error( 'Error:', error ) )
		.then( text => {
			postsGridElement.classList.remove( 'loading' );

			postsGridElement.querySelector( '.posts-grid__posts' ).innerHTML = text;

			const postGridElements = document.querySelectorAll( '.post-grid' );

			if ( postGridElements ) {
				[].forEach.call( postGridElements, function( postGridElement ) {
					postGridElement.classList.add( 'is-in-viewport' );
				} );
			}
		} )
		.catch( error => console.log( { DownloadError: error } )
	);
}


/**
 * Each Forms
 */
const postsGridElements = document.querySelectorAll( '.posts-grid' );

if ( postsGridElements ) {
	[].forEach.call( postsGridElements, function( postsGridElement ) {
		const postsFridForm = postsGridElement.querySelector( '.posts-grid__form' );

		if ( postsFridForm ) {

			// Filters
			const formFilters = document.querySelectorAll( '.filter' );

			if ( formFilters ) {
				[].forEach.call( formFilters, function( formFilter ) {
					formFilter.addEventListener('click', function() {
						if ( formFilter.classList.contains( 'filter--active' ) ) {
							formFilter.classList.remove( 'filter--active' );
						} else {
							formFilter.classList.add( 'filter--active' );
						}
					} );
				} );
			}

			// Checkboxes
			const formCheckboxes = postsFridForm.querySelectorAll( 'input[type=checkbox]' );

			if ( formCheckboxes ) {
				[].forEach.call( formCheckboxes, function( formCheckbox ) {
					formCheckbox.addEventListener('change', function() {
						postsGridRestData( postsGridElement );
					} );
				} );
			}

		}
	} );
}
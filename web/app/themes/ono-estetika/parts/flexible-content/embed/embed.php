<?php
//ACF

$section_title = get_sub_field('section_title');
$description = get_sub_field('section_description');
$embed =  get_sub_field('embed');
$section_id = get_sub_field('section_id');
?>

<section <?php echo $section_id ? 'id="'. $section_id .'"' : ''?> class="embed">
    <?php load_inline_styles(__DIR__, 'embed'); ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-10">
                <?php load_inline_styles(__DIR__, 'embed'); ?>
                <?php if ($section_title) : ?>
                    <h2 class="embed__title has-line-animation has-gradient"><?php echo $section_title ?></h2>
                <?php endif ?>
                <?php if ($description) : ?>
                    <div class="embed__description has-line-animation"><?php echo $description ?></div>
                <?php endif ?>
                <?php if ($embed) : ?>
                    <div class="embed__iframe"><?php echo $embed ?></div>
                <?php endif ?>
            </div>
        </div>
        <div class="embed__decoration">
            <div class="decoration">
                <?php echo get_img('line-decoration-large'); ?>
            </div>
        </div>
    </div>
</section>
<?php

/**
 * Single Blog Post template.
 *
 * @package    WordPress
 * @subpackage ono-estetika
 * @since      ono-estetika 1.0
 */
get_header();
the_post();
?>
<main id="page-content" role="main" class="page-content page-content--single">
	<div id="content" tabindex="-1" class="page-content__wrapper">
		<?php get_template_part('parts/flexible-content/flexible'); ?>
	</div>
</main>
<?php
get_footer();

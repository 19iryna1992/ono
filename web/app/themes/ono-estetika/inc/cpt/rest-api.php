<?php

/**
 * Rest Api Init
 */
add_action( 'rest_api_init', function () {
	register_rest_route( 'posts-grid/v1', '/posts', [
		'methods'             => 'POST',
		'callback'            => 'posts_grid_enquire_posts',
		'permission_callback' => '__return_true'
	] );
} );


/**
 * Posts Grid Enquire Posts
 */
function posts_grid_enquire_posts( $request ) {
	$name       = 'posts-grid';
	$query_args = [
		'post_status' => 'publish'
	];
	$tax_args   = [];
	$meta_args  = [];
	$form_data  = (array) json_decode( $request->get_body() );
	$post_type  = ! empty( $form_data['post_type'] ) ? esc_attr( $form_data['post_type'] ) : '';



	// Default Args
	foreach ( [
		'post_type'
	] as $key ) {
		if ( ! empty( $form_data[$key] ) ) {
			$query_args[$key] = esc_attr( $form_data[$key] );
		}
	}

	// Meta Args
	foreach ( [
		'clinic',
		'clinics',
		'treatments',
	] as $key ) {
		if ( ! empty( $form_data[$key] ) ) {
			foreach ( (array) $form_data[$key] as $value ) {
				$meta_args[] = [
					'key'	 	=> esc_attr( $key ),
					'value'	  	=> esc_attr( $value ),
					'compare' 	=> 'LIKE',
				];
			}
		}
	}

	// Tax Args
	foreach ( [
		'treatment-gender',
		'treatment-area',
		'treatment-type',
		'treatment-goal',
		'goal-gender',
		'goal-area',
		'goal-benefit',
		'doctor-area',
		'doctor-language-speaking'
	] as $key ) {
		if ( ! empty( $form_data[$key] ) ) {
			$tax_args[] = [
				'taxonomy' => esc_attr( $key ),
				'terms'    => wp_parse_id_list( $form_data[$key] )

			];
		}
	}


	// Meta Query
	if ( ! empty( $meta_args ) ) {
		$query_args['meta_query'] = array_merge(
			['relation' => 'AND'],
			$meta_args
		);
	}

	// Tax Query
	if ( ! empty( $tax_args ) ) {
		$query_args['tax_query'] = array_merge(
			['relation' => 'AND'],
			$tax_args
		);
	}


	// Orderby
	if ( $post_type == 'doctor' ) {
		$query_args['meta_key'] = 'last_name';
		$query_args['orderby']  = 'meta_value';
		$query_args['order']    = 'ASC';
	}

	$block_query = new WP_Query( $query_args );

	if ( ! empty( $post_type ) && $block_query->have_posts() ) :
		?><div class="<?php esc_attr_e( $name ); ?>__results"><?php
				$found_posts = $block_query->found_posts;

				echo sprintf( '%s %s %s',
					$found_posts,
					_n($post_type, $post_type . 's', $found_posts, 'ono-estetika'),
					_n('found', 'founds', $found_posts, 'ono-estetika')
				);
			?></div>
			<div class="<?php esc_attr_e( $name ); ?>__row row">
				<?php while ( $block_query->have_posts() ) : $block_query->the_post(); ?>
					<?php echo get_part( 'archive/grid/' .  $post_type ); ?>
				<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>
			</div>
		<?php
	else:
		?><div class="<?php esc_attr_e( $name ); ?>__results <?php esc_attr_e( $name ); ?>__results--empty"><?php
			_e( 'Sorry, nothing found.', 'ono-estetika' );
		?></div><?php
	endif;

	die();
}
<?php

/**
 * Register Custom Post Type
 */
function register_cpt() {
	$all_post_types = apply_filters( 'theme_cpt', array() );

	if ( ! empty( $all_post_types ) ) {
		foreach ( $all_post_types as $args ) {
			$default_settings = array(
				'labels'             => array(
					'name'           => _x( $args['multiple_name'], 'post type general name', 'ono-estetika' ),
					'singular_name'  => _x( $args['singular_name'], 'post type singular name', 'ono-estetika' ),
					'menu_name'      => _x( $args['multiple_name'], 'admin menu', 'ono-estetika' ),
					'name_admin_bar' => _x( $args['singular_name'], 'add new on admin bar', 'ono-estetika' ),
					'all_items'      => $args['multiple_name'],
				),
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_rest'       => true,
				'has_archive'        => false,
				'exclude_from_search'=> false,
				'hierarchical'       => false,
				'rewrite'            => array( 'with_front' => false ),
				'query_var'          => true,
				'menu_position'      => 5,
				'show_in_menu'       => true,
				'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
			);

			$post_type_args = array_merge( $default_settings, $args );

			unset( $post_type_args['post_type'] );

			register_post_type( $args['post_type'], $post_type_args );

			if ( update_option( 'theme_cpt_' . $args['post_type'], true ) ) {
				flush_rewrite_rules();
			}
		}
	}
}
add_action( 'init', 'register_cpt' );


/**
 * Register Custom Taxonomies
 */
function register_taxonomies() {
	$all_taxonomies = apply_filters( 'theme_tax', array() );

	if ( ! empty( $all_taxonomies ) ) {
		foreach ( $all_taxonomies as $args ) {
			$default_settings = array(
				'labels'             => array(
					'name'          => _x( $args['multiple_name'], 'taxonomy general name', 'ono-estetika' ),
					'menu_name'     => __( $args['multiple_name'], 'ono-estetika' ),
					'search_items'  => __( 'Search ' . $args['multiple_name'], 'ono-estetika' ),
					'singular_name' => _x( $args['singular_name'], 'taxonomy singular name', 'ono-estetika' ),
					'all_items'     => $args['multiple_name'],
				),
				'hierarchical'       => true,
				'show_ui'            => true,
				'show_in_rest'       => true,
				'show_admin_column'  => true,
				'query_var'          => true,
				'publicly_queryable' => true,
				'rewrite'            => array( 'slug' => $args['tax_slug'], 'with_front' => false )
			);

			$tax_args = array_merge( $default_settings, $args );

			register_taxonomy( $args['tax_slug'], $args['for_types'], $tax_args );
		}
	}

	register_taxonomy_for_object_type( 'period', 'attachment');
}
add_action( 'init', 'register_taxonomies' );


/**
 * Custom Taxonomies Filter
 */
add_filter( 'theme_tax', function( $taxonomies ) {

	// Treatment Type
	$taxonomies[] = array(
		'tax_slug'      => 'treatment-gender',
		'for_types'     => array( 'treatment' ),
		'singular_name' => 'Gender',
		'multiple_name' => 'Genders',
	);

	// Treatment Area
	$taxonomies[] = array(
		'tax_slug'      => 'treatment-area',
		'for_types'     => array( 'treatment' ),
		'singular_name' => 'Area',
		'multiple_name' => 'Areas',
	);

	// Treatment Type
	$taxonomies[] = array(
		'tax_slug'      => 'treatment-type',
		'for_types'     => array( 'treatment' ),
		'singular_name' => 'Type',
		'multiple_name' => 'Types',
	);

	// Treatment Goal
	$taxonomies[] = array(
		'tax_slug'      => 'treatment-goal',
		'for_types'     => array( 'treatment' ),
		'singular_name' => 'Goal',
		'multiple_name' => 'Goals',
	);

	// Goal Type
	$taxonomies[] = array(
		'tax_slug'      => 'goal-gender',
		'for_types'     => array( 'goal' ),
		'singular_name' => 'Gender',
		'multiple_name' => 'Genders',
	);

	// Goal Area
	$taxonomies[] = array(
		'tax_slug'      => 'goal-area',
		'for_types'     => array( 'goal' ),
		'singular_name' => 'Area',
		'multiple_name' => 'Areas',
	);

	// Goal Benefit
	$taxonomies[] = array(
		'tax_slug'      => 'goal-benefit',
		'for_types'     => array( 'goal' ),
		'singular_name' => 'Benefit',
		'multiple_name' => 'Benefits',
	);

	// Doctor Area
	$taxonomies[] = array(
		'tax_slug'      => 'doctor-area',
		'for_types'     => array( 'doctor' ),
		'singular_name' => 'Area',
		'multiple_name' => 'Areas',
	);

	// Doctor Benefit
	$taxonomies[] = array(
		'tax_slug'      => 'doctor-language-speaking',
		'for_types'     => array( 'doctor' ),
		'singular_name' => 'Language Speaking',
		'multiple_name' => 'Languages',
	);

	return $taxonomies;
} );


/**
 * Custom Post Type Filter
 */
add_filter( 'theme_cpt', function( $post_types ) {

	// Doctor
	$post_types[] = array(
		'post_type'     => 'doctor',
		'singular_name' => 'Doctor',
		'multiple_name' => 'Doctors',
		'menu_icon'     => 'dashicons-nametag',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'    => array( 'doctor-area', 'doctor-language-speaking'  ),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	// Clinic
	$post_types[] = array(
		'post_type'     => 'clinic',
		'singular_name' => 'Clinic',
		'multiple_name' => 'Clinics',
		'menu_icon'     => 'dashicons-building',
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'    => array(),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	// Treatment
	$post_types[] = array(
		'post_type'     => 'treatment',
		'singular_name' => 'Treatment',
		'multiple_name' => 'Treatments',
		'menu_icon'     => 'dashicons-editor-paste-text',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'    => array( 'treatment-gender', 'treatment-area', 'treatment-type', 'treatment-goal' ),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	// Goals
	$post_types[] = array(
		'post_type'     => 'goal',
		'singular_name' => 'Goal',
		'multiple_name' => 'Goals',
		'menu_icon'     => 'dashicons-chart-bar',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'    => array( 'goal-gender', 'goal-area', 'goal-benefit' ),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	// Testimonial
	$post_types[] = array(
		'post_type'     => 'testimonial',
		'singular_name' => 'Testimonial',
		'multiple_name' => 'Testimonials',
		'menu_icon'     => 'dashicons-testimonial',
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'    => array(),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	// FAQ
	$post_types[] = array(
		'post_type'     => 'faq',
		'singular_name' => 'FAQ',
		'multiple_name' => 'FAQ',
		'menu_icon'     => 'dashicons-megaphone',
		'supports'      => array( 'title', 'editor'),
		'taxonomies'    => array(),
		'rewrite'       => array(
			'with_front' => false
		)
	);

	return $post_types;
} );
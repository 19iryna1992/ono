<?php
/*
|--------------------------------------------------------------------------
| Init theme base scripts
|--------------------------------------------------------------------------
|
*/
require_once 'base/init.php';

/*
|--------------------------------------------------------------------------
| Include All Inc Files
|--------------------------------------------------------------------------
|
*/
foreach ( glob(__DIR__ . '/inc/*/*.php') as $filename )
{
	require_once( $filename );
}

/*
|--------------------------------------------------------------------------
| Register Gutenberg Blocks
|--------------------------------------------------------------------------
|
*/
if( function_exists('acf_register_block_type') ) {
	add_action('acf/init', 'register_acf_blocks');
}

function register_acf_blocks() {
	foreach ( glob(__DIR__ . '/parts/gutenberg/*/register.php') as $filename )
	{
		require_once( $filename );
	}
}
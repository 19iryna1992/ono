<?php
function load_blocks_script($path, $name, $deps = [], $base = 'parts/components') {
    $file = get_template_directory_uri() . "/$base/$path/index.min.js";

	$arr = [
		'name' => $name,
		'file' => $file,
		'deps' => $deps
	];

	add_action( 'wp_footer', function() use ($arr) {
		wp_enqueue_script(
			$arr['name'],
			$arr['file'],
			$arr['deps'],
			false,
			true
		);
	}, 1);
}
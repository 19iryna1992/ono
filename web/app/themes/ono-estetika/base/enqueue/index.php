<?php
/*
|--------------------------------------------------------------------------
| Load Main Scripts/Styles & localize vars to JS
|--------------------------------------------------------------------------
*/
function enqueue_theme_scripts() {
	global $custom_settings;
	$deps = ['rellax'];

	if ( isset( $custom_settings->jquery ) && $custom_settings->jquery == true ) {
		$deps[] = 'jquery';
	}

	wp_enqueue_script(
		'script-load',
		get_template_directory_uri() . '/assets/js/bundle-load.min.js',
		$deps,
		filemtime( get_template_directory() . '/assets/js/bundle-load.min.js' ),
		true
	);
	wp_enqueue_script(
		'script',
		get_template_directory_uri() . '/assets/js/bundle.min.js',
		$deps,
		filemtime( get_template_directory() . '/assets/js/bundle.min.js' ),
		true
	);

	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/style.css', false );

	$vars = array(
		'templateUrl' => get_template_directory_uri(),
		'ajaxUrl'     => site_url() . '/wp-admin/admin-ajax.php',
		'restUrl'     => get_rest_url()
	);

	wp_localize_script( "script", "vars", $vars );

}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts');

/*
|--------------------------------------------------------------------------
| Load Admin Styles
|--------------------------------------------------------------------------
*/
function add_custom_admin_styles() {
	wp_enqueue_style( 'style-admin', get_template_directory_uri() . '/assets/css/style-admin.css' );
}
add_action( 'admin_enqueue_scripts', 'add_custom_admin_styles' );


/*
|--------------------------------------------------------------------------
| Enqueue Third Party Blocks Assets
|--------------------------------------------------------------------------
*/
function enqueue_custom_blocks_assets() {
	global $custom_settings;

	if ( isset( $custom_settings->register_scripts ) ) {
		foreach ( $custom_settings->register_scripts as $key => $value ) {
			wp_register_script(
				$key,
				get_template_directory_uri() . $value,
				[],
				false,
				true
			);
		}
	}
}
add_action( 'enqueue_block_assets', 'enqueue_custom_blocks_assets' );
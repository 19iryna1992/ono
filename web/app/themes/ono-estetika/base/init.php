<?php
/*
|--------------------------------------------------------------------------
| Store Custom Theme Settings File
|--------------------------------------------------------------------------
|
*/
global $custom_settings;
$custom_settings = json_decode( preg_replace( '/\/\*(?s).*?\*\//', '', file_get_contents( get_template_directory() . '/custom-settings.json' ) ) );

if ( ! $custom_settings ) {
	add_action( 'admin_notices', function () {
		echo "<div class='error'><p>custom-settings.json is invalid</p></div>";
	} );
}

/*
|--------------------------------------------------------------------------
| Include All Base Files
|--------------------------------------------------------------------------
|
*/
foreach ( glob(__DIR__ . '/*/*.php') as $filename )
{
	require_once($filename);
}
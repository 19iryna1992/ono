<?php
/*
|--------------------------------------------------------------------------
| Get Inline Image
|--------------------------------------------------------------------------
*/
function get_svg_tag( $el ) {
    $svg = file_get_contents( $el );
    preg_match( '/<svg[\s\S]*\/svg>/m', $svg, $matches );
	$return = isset( $matches[0] ) ? $matches[0] : $svg;
    return $return;
}

function get_img( $id, $thumb = 'full', $dir = '/assets/images/svg/' ) {
	$return = '';

	if ( is_int( $id ) ) {
		$src = wp_get_attachment_image_src( $id, $thumb );

		if ( ! $src ) { return ''; }

		$ext = pathinfo( $src[0], PATHINFO_EXTENSION );

		if ( $ext != 'svg' ) { return wp_get_attachment_image( $id, $thumb ); }

		$img = realpath( get_attached_file( $id, true ) );
		$return = $img ? get_svg_tag( $img ) : '';
	} else {
		$file = get_template_directory() . $dir . $id . '.svg';
		$return = file_exists( $file ) ? get_svg_tag( $file ) : '';
	}

	return $return;
}
<?php
function check_styles_urls($string) {
	$path = get_template_directory_uri() . '/assets/images/';
	$url = "url($path$3)";
	$pattern = '/url\s*\(\s*[\'"]?(?!(((?:https?:)?\/\/)|(?:data\:?:)|(?:#)))([^\'"\)]+)[\'"]?\s*\)/i';
	return preg_replace($pattern, $url, $string);
}

<?php

function make_phone_clickable( $phone ) {
	$number = trim( preg_replace( '/[^\d|\+]/', '', $phone ) );

	return $number ? '<a href="tel:' . esc_attr( $number ) . '">' . esc_html( $phone ) . '</a>' : $phone;
}

function make_email_clickable( $email ) {
	return '<a href="mailto:' . esc_attr( $email ) . '">' . esc_html( $email ) . '</a>';
}
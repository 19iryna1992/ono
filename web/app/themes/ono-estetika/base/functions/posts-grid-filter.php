<?php

function the_posts_grid_filter( $post_type = '', $filter_type = '', $meta_name = '', $post_type_name = '', $is_opened = false ) {
	$filter_names = [
		'gender'            => __( 'Gender', 'ono-estetika' ),
		'area'              => __( 'Areas', 'ono-estetika' ),
		'benefit'           => __( 'Benefits', 'ono-estetika' ),
		'goal'              => __( 'Goals', 'ono-estetika' ),
		'type'              => __( 'Types', 'ono-estetika' ),

		'clinic'            => __( 'Clinic', 'ono-estetika' ),
		'language-speaking' => __( 'Language Speaking', 'ono-estetika' ),
		'clinics'           => __( 'Clinics', 'ono-estetika' ),
		'treatments'        => __( 'Treatments', 'ono-estetika' ),
	];

	$checkbox_options = [
		// 'language_speaking' => [
		// 	'en' => __( 'English', 'ono-estetika' ),
		// 	'fr' => __( 'French', 'ono-estetika' ),
		// ]
	];

	switch ( $filter_type ) {
		case 'taxonomy':
			$taxonomy = sprintf( '%s-%s', $post_type, $meta_name );
			$terms    = get_terms([
				'taxonomy'   => $taxonomy,
				'hide_empty' => false
			]);

			?><div class="filter block-show--up <?php echo $is_opened ? 'filter--active' : '';?>"><?php
				if ( ! empty( $filter_names[$meta_name] ) ) {
					?><div class="filter__title"><span><?php echo $filter_names[$meta_name]; ?></span><?php echo get_img( 'dropdown' ); ?></div><?php
				}

				?><div class="filter__item__list"><?php
					if ( ! empty( $terms ) ) {
						foreach ( $terms as $term ) {
							?>
								<label class="filter__item">
									<input class="filter__item__checkbox" type="checkbox" name="<?php esc_attr_e( $taxonomy ); ?>" value="<?php esc_attr_e( $term->term_id ); ?>">
									<span><?php echo $term->name; ?></span>
								</label>
							<?php
						}
					}
				?></div><?php
			?></div><?php

			break;

		case 'checkbox':
			?><div class="filter block-show--up <?php echo $is_opened ? 'filter--active' : '';?>"><?php
				if ( ! empty( $filter_names[$meta_name] ) ) {
					?><div class="filter__title"><span><?php echo $filter_names[$meta_name]; ?></span><?php echo get_img( 'dropdown' ); ?></div><?php
				}

				?><div class="filter__item__list"><?php

					if ( ! empty( $checkbox_options[$meta_name] ) ) {
						foreach ( $checkbox_options[$meta_name] as $option_key => $option_value ) {
							?>
								<label class="filter__item">
									<input class="filter__item__checkbox" type="checkbox" name="<?php esc_attr_e( $meta_name ); ?>" value="<?php esc_attr_e( $option_key ); ?>">
									<span><?php echo $option_value; ?></span>
								</label>
							<?php
						}
					}
				?></div><?php
			?></div><?php

			break;

		case 'post_type':
			$posts = get_posts( [
				'post_type'      => $post_type_name,
				'posts_per_page' => -1
			] );

			?><div class="filter block-show--up <?php echo $is_opened ? 'filter--active' : '';?>"><?php
				if ( ! empty( $filter_names[$meta_name] ) ) {
					?><div class="filter__title"><span><?php echo $filter_names[$meta_name]; ?></span><?php echo get_img( 'dropdown' ); ?></div><?php
				}

				?><div class="filter__item__list"><?php
					if ( ! empty( $posts ) ) {
						foreach ( $posts as $post ) {
							?>
								<label class="filter__item">
									<input class="filter__item__checkbox" type="checkbox" name="<?php esc_attr_e( $meta_name ); ?>" value="<?php esc_attr_e( $post->ID ); ?>">
									<span><?php echo $post->post_title; ?></span>
								</label>
							<?php
						}
					}
				?></div><?php
			?></div><?php
			break;

	}
}
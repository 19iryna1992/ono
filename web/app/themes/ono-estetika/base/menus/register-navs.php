<?php
function register_theme_nav_menus() {
	global $custom_settings;
	if ( isset( $custom_settings->menus ) ) {
		$nav_array = array();

		foreach ( $custom_settings->menus as $key => $value ) {
			$nav_array[ $key ] = $value;
		}
		register_nav_menus( $nav_array );
	}
}
add_action( 'init', 'register_theme_nav_menus', 0 );
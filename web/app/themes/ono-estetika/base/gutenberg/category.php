<?php
function register_theme_block_categories( $categories ) {
	global $custom_settings;

	if ( isset( $custom_settings->gutenberg_categories ) ) {
		$cat_array = array();

		foreach ( $custom_settings->gutenberg_categories as $cat ) {
			$cat_array[] = array(
				'slug' => $cat[0],
				'title' => __($cat[1], 'ono-estetika'),
				'icon'  => $cat[2],
			);
		}

		return array_merge(
			$cat_array,
			$categories
		);
	}
}
add_filter( 'block_categories_all', 'register_theme_block_categories', 10, 2 );
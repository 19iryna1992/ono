<?php
/*
|--------------------------------------------------------------------------
| Add Theme Support
|--------------------------------------------------------------------------
*/
function add_base_support() {
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'wp-block-styles' );
	add_theme_support( 'align-wide' );
	add_theme_support( 'align-full' );
	add_theme_support( 'editor-styles' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'editor-font-sizes', array(
		array(
			'name' => __( 'Normal', 'ono-estetika' ),
			'size' => 16,
			'slug' => 'normal'
		),
		array(
			'name' => __( 'Large', 'ono-estetika' ),
			'size' => 24,
			'slug' => 'large'
		)
	) );
	add_editor_style( 'assets/css/style-editor.css' );
	add_theme_support( 'editor-color-palette', settings_colors() );
}
add_action( 'after_setup_theme', 'add_base_support' );

function settings_colors($return_format = 'support') {
	$colors = json_decode(file_get_contents(get_template_directory() . '/colors.json'), true);
	if ($return_format === 'support') {
		$arr = [];
		foreach ($colors as $key => $color) {
			$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $key)));
			$arr[] = array(
				'name' => __( $key, 'ono-estetika' ),
				'slug' => $slug,
				'color' => $color,
			);
		}
		return $arr;
	} else if ($return_format === 'styles') {
		$html = '<style type="text/css">';
		foreach ($colors as $key => $color) {
			$slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $key)));
			$html .= ".has-$slug-color{color:$color;}";
			$html .= ".has-$slug-background-color{background-color:$color;}";
		}
		$html .= '</style>';
		echo $html;
	}
}
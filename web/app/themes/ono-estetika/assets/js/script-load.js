"use strict";


/**
 * Fix vh variable
 */
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

window.addEventListener('resize', () => {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
});


/**
 * Has Line Animation Wrapp
 */
const wrapElements = document.querySelectorAll( '.has-line-animation--wrap > p' );

if ( wrapElements ) {
	[].forEach.call( wrapElements, function( wrapElement ) {
		wrapElement.classList.add( 'has-line-animation' );
	} );
}


/**
 * Has Line Animation
 */
const lineElements = document.querySelectorAll( '.has-line-animation' );

if ( lineElements ) {
	[].forEach.call( lineElements, function( lineElement ) {

		// Animation Lines Wrap Words
		const blockWords = lineElement.textContent.trim().split(' ');

		if ( blockWords ) {
			let outputText = '';

			[].forEach.call( blockWords, function( text ) {
				const maybeBrText = text.trim().split('\u2028\n'),
					  maybeNText  = text.trim().split('\n');

				if ( maybeBrText.length > 1 ) {
					[].forEach.call( maybeBrText, function( text, i ) {
						if ( i > 0 && i < maybeBrText.length ) {
							outputText = outputText + '<br>';
						}
						outputText = outputText + `<span><span>${text.trim()}</span></span> `;
					} );
				}
				else if ( maybeNText.length > 1 ) {
					[].forEach.call( maybeNText, function( text, i ) {
						if ( i > 0 && i < maybeNText.length ) {
							outputText = outputText + '<br>';
						}
						outputText = outputText + `<span><span>${text.trim()}</span></span> `;
					} );

				}
				else {
					outputText = outputText + `<span><span>${text.trim()}</span></span> `;
				}
			} );

			lineElement.innerHTML = outputText;
		}


		// Fix Gradient Colors
		if ( lineElement.classList.contains( 'has-gradient' ) ) {
			const gradientSpanElements = lineElement.querySelectorAll( ':scope > span' );

			if ( gradientSpanElements ) {
				let line    = 0,
					prevTop = -15;

				[].forEach.call( gradientSpanElements, function( spanElement ) {
					const top = spanElement.offsetTop;

					if ( top != prevTop ) {
						prevTop = top;

						line++;
					}
					spanElement.setAttribute( 'class', 'line line--' + line );
				} );

				let maxBgSize = 0;

				for( let i = 1; i <= line; i++ ) {
					let allColorElemsWidth = 0;

					const lineSpanElements = lineElement.querySelectorAll( ':scope > span.line--' + i );

					if ( lineSpanElements ) {
						[].forEach.call( lineSpanElements, function( lineSpanElement ) {
							const colorSpanChildElement    = lineSpanElement.querySelector( ':scope > span' ),
								colorSpanChildElementWidth = lineSpanElement.offsetWidth;

							colorSpanChildElement.innerHTML = `<span class="color" style="background-position: -${allColorElemsWidth}px 0;">${colorSpanChildElement.textContent}</span>`;

							allColorElemsWidth += parseInt( colorSpanChildElementWidth );
						} );
					}

					if ( allColorElemsWidth > maxBgSize ) {
						maxBgSize = allColorElemsWidth;
					}
				}

				for( let i = 1; i <= line; i++ ) {
					const lineSpanElements = lineElement.querySelectorAll( ':scope > span.line--' + i );

					if ( lineSpanElements ) {

						[].forEach.call( lineSpanElements, function( lineSpanElement ) {
							const colorSpanChildElement = lineSpanElement.querySelector( ':scope > span > span' );

							colorSpanChildElement.style.backgroundSize = `${maxBgSize}px 100%`;
						} );
					}
				}
			}
		}

	} );
}


/**
 * In View Observer
 */
const inViewObserver = new IntersectionObserver( nodes => {
	nodes.forEach( node => {
		if (node.isIntersecting) {
			node.target.classList.add( 'is-in-viewport' );

			inViewObserver.unobserve( node.target );
		}
	})
}, { threshold: .5 });

const contentBlocks = document.querySelectorAll( '.has-line-animation, .event-viewport, [class*="block-show"]' );

[].forEach.call( contentBlocks, function( contentBlock ) {
	inViewObserver.observe( contentBlock );
} );



/**
 * Add Loaded class to Body
 */
window.addEventListener( 'DOMContentLoaded', () => {
	document.querySelector( 'body' ).classList.add( 'dom-content-loaded' );
} );
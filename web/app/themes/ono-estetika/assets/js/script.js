/**
 * Parallax IMG
 */
 new Rellax( '.rellax', {
	center: true
} );


/**
 * Parallax Hero IMG
 */
new Rellax( '.rellax__bg', {
	center: true,
	callback: function(positions) {
		if ( positions.y < 0 ) {
			const rellaxBg     = document.getElementsByClassName( 'rellax__bg' ),
				  rellaxHeight = rellaxBg[0].offsetHeight / 4.2;

			let addPercent =(rellaxHeight + positions.y ) / 10;

			rellaxBg[0].style.clipPath = `circle(${66 + addPercent}% at 50% -30%)`;
		}
	}
} );


/**
 * Parallax BG
 */
new Rellax( '.parallax__bg > *', {
	speed: -3,
	center: true
} );


/**
 * Animate Line Decoradion
 */
const percentageSeen = ( element ) => {
	const viewportHeight = window.innerHeight,
		scrollTop        = window.scrollY,
		elementRect      = element.getBoundingClientRect(),
		elementOffsetTop = elementRect.top + document.documentElement.scrollTop,
		elementHeight    = elementRect.height,
		distance         = scrollTop + viewportHeight - elementOffsetTop - 200,
		percentage       = Math.round(
			distance / (((viewportHeight / 2) + elementHeight) / 100)
		);

	return Math.min(100, Math.max(0, percentage));
};


const drawSVGLine = function() {
	const svgPaths = document.querySelectorAll( '.svg-path-line' );

	[].forEach.call( svgPaths, function( svgPath ) {
		let percentageComplete = percentageSeen( svgPath.parentElement.parentElement ),
			newUnit    = parseInt( svgPath.style.strokeDasharray, 10 ),
			offsetUnit = newUnit - percentageComplete * (newUnit / 100);

		svgPath.style.strokeDashoffset = `${offsetUnit}px`;
	} );
}

drawSVGLine();

window.addEventListener( 'scroll', drawSVGLine );


const s = document.querySelectorAll('a[href^="#"]');
s && s.forEach((e => {
	e.addEventListener("click", (t => {
		t.preventDefault();
		const n = e.getAttribute("href"),
			i = document.querySelector(n);
		if (i) {
			const e = i.getBoundingClientRect().top + window.pageYOffset - 70;
			setTimeout((() => window.scrollTo({
				top: e,
				behavior: "smooth"
			})))
		}
	}))
}));